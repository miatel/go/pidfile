package pidfile

import (
	"errors"
	"os"
	"strconv"
	"syscall"
)

var EmptyFileName = errors.New("empty PID file name")

func Write(fileName string) error {
	var (
		err error
		f   *os.File
	)
	if fileName == "" {
		return EmptyFileName
	}
	if f, err = os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, 0640); err != nil {
		return err
	}
	if err = syscall.Flock(int(f.Fd()), syscall.LOCK_EX|syscall.LOCK_NB); err != nil {
		goto err
	}
	if err = f.Truncate(0); err != nil {
		goto err
	}
	if _, err = f.Write([]byte(strconv.Itoa(os.Getpid()))); err != nil {
		goto err
	}
	if err = f.Sync(); err != nil {
		goto err
	}
	return nil

err:
	f.Close()
	return err
}

func Unlink(fileName string) {
	if fileName != "" {
		_ = os.Remove(fileName)
	}
}
